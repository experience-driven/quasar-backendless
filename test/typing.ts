import * as qb from '../'

class DBContact {
  public objectId?:  string;
  public firstName?: string;
  public lastName?:  string;
  public friends?:   DBContact;
};

class Contact extends DBContact {
  public get fullName() { return this.firstName + " " + this.lastName; }
}

qb.mapTableToModel('Contact', Contact); // *might* be needed for related columns?

const db = qb.table<DBContact, Contact>('Contact');

db.fetch().then(results => {
  for (const contact of results) {
    console.log(contact.fullName);
  }
});