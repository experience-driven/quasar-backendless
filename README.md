quasar-backendless
===

Common tools for Quasar+Backendless stack

# Install
1. Install via npm:
```bash
npm i quasar-backendless
```

2. In your project's boot folder, create a file called `quasar-backendless.js` and enter the following code in it:

```js
import qbBoot from 'quasar-backendless/boot'
export default qbBoot
```

3. In your project's root folder, edit `quasar.conf.js` and add `quasar-backendless` to the `boot` config:

```js
boot: [
  ...
  'quasar-backendless', // (name of the boot file you've made)
  ...
]
```

4. Run `quasar dev` on your project.

# Updates

Manual updating:
```bash
npm update quasar-backendless
```

Automated updates on project launch:

1. Update your project's `package.json` by adding an update-then-run script:

```js
"scripts": {
  ...
  "dev": "npm update quasar-backendless && quasar dev",
  "build": "npm update quasar-backendless && quasar build",
  ...
},
```

2. Use `npm run dev` or `npm run build` in place of `quasar dev|build` to check for updates before launching the project app.

# Uninstall
```bash
npm uninstall quasar-backendless
```

# Usage - Examples

Once quasar-backendless is installed, you can access it's API like this:

```js
import * as qb from 'quasar-backendless'
// qb.init()
// qb.table(...)
// etc ...
```

To use the components packaged in quasar-backendless:

```js
<template>
  <q-page>
    <qb-table flat
      table="Person"
      :columns="[
        { name:'firstName', field:'firstName', label:'First Name' },
        { name:'lastName',  field:'lastName',  label:'Last Name' },
      ]"
    />
  </q-page>
</template>

<script>
import * as qb from 'quasar-backendless'
import { QBTable } from 'quasar-backendless/components'

export default {
  components: { QBTable },
  setup(props, context) {
    const local = qb.init();
    // ...
    return { ...local };
  },
}
</script>
```