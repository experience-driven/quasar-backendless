import { getCurrentInstance, onUnmounted } from 'vue'
import { useQuasar, QVueGlobals } from 'quasar'
import Backendless from 'backendless'


import State from './state/State'
import LoadingManager from './state/LoadingManager'
import TableQuery from './data/TableQuery'
import TableViewModel from './data/TableViewModel'

export {
  State,
  LoadingManager,
  TableQuery,
  TableViewModel,
};


/**
 * Name of the extension.
 */
export const name = "quasar-backendless";


/**
 * Full name of the extension.
 */
export const fullName = "quasar-backendless";


/**
 * @typedef QBInit
 * @property {qb} qb - Quasar-Backendless
 * @property {object} app - Vue app context global properties
 * @property {QVueGlobals} q - Quasar global object
 * @property {isLoading} isLoading - Get loading state scoped for this component.
 * @property {setLoading} setLoading - Set loading state scoped for this component.
 */
/*****************************************************************************
 * Initialize quasar-backendless related services for a Vue component.
 * **Use in setup() only.**  
 * Can be used in setup() return object to expose to templates:
 * ```vue
 * <template>
 *   <div v-if="isLoading()">...</div>
 * </template>
 * <script>
 * export default {
 *   setup() {
 *     return { ...qb.init(), other, stuff }
 *   }
 * }
 * </script>
 * ```
 * @returns {QBInit}
 */
export function init() {
  const qb = State.getInstance();
  const instance = getCurrentInstance();

  if (instance == null)
    throw new Error("Could not get current instance. Was init() called in setup()?");

  onUnmounted(() => qb.unmap(instance));

  const app = instance?.appContext.config.globalProperties;
  const q = useQuasar();

  const isLoading = function(key?: string) {
    let mc = qb.mapped(instance);
    return mc.loader.loading(key).value;
  };

  const setLoading = function(key: string, state: boolean) {
    let mc = qb.mapped(instance);
    return mc.loader.set(key, state).value;
  };

  return { app, q, isLoading, setLoading };
}


/*******************************************************************************
 * @param tableName Name of the table.
 * @param globalCondition Where-clause condition applied to all
 * fetch calls on this table.
 * @returns New TableViewModel for the Backendless data table `tableName`.
 */
export function table<DBModel = object, ViewModel extends DBModel = DBModel>(
  tableName:        string,
  globalCondition?: string
): TableViewModel<DBModel, ViewModel> {
  let table = new TableViewModel<DBModel, ViewModel>(tableName);
  if (globalCondition != null) {
    table = table.setGlobalCondition(globalCondition);
  }
  return table;
}


/*******************************************************************************
 * 
 */
export function mapTableToModel<T extends object>(
  tableName: string,
  viewModel: new () => T
): void {
  Backendless.Data.mapTableToClass(tableName, viewModel);
}


/*****************************************************************************
 * Get the app's global loading state.
 * Getting a key state applies only to the global loading state.
 * Use `isLoading()` returned by `init()` for component-scoped loading state.
 * 
 * @param {string} [key] If provided, check the state for that key, only
 * globally. If not provided, the function will return true if there is any
 * active loading state, either globally or in any `init()`'d components.
 * 
 * @example
 * qb.isLoading("mySmallTask") // Get the loading state for "mySmallTask".
 * qb.isLoading() // Returns true if there is any loading, either globally or by any components.
 * 
 * @returns {boolean} Current state of given key. If no key is provided,
 * returns true if any state is active.
 */
export function isLoading(key?: string): boolean {
  return State.getInstance().loading(key).value;
}


/*****************************************************************************
 * Set the app's global loading state.
 * Setting a key state applies only to the global loading state.
 * Use `setLoading()` returned by `init()` for component-scoped loading state.
 * 
 * @param {string} key Set the state for that key, only globally.
 * @param {boolean} state New loading state.
 * 
 * @example
 * qb.loading("mySmallTask", true) // Enables loading state for key "mySmallTask".
 * 
 * @returns {boolean} Current state of the given key.
 */
export function setLoading(key: string, state: boolean): boolean {
  return State.getInstance().loading(key, state).value;
}


/*****************************************************************************
 * Similar to qb.isLoading(), but only checks the global loading state,
 * ignoring if any components have active loading states.
 * 
 * @returns {boolean} Current loading state, exclusively global.
 */
export function isLoadingGlobalOnly(): boolean {
  return State.getInstance().loadingGlobalOnly().value;
}


/*****************************************************************************
 * Create a new query.
 * 
 * Alias for `new qb.TableQuery(null)`.
 * 
 * Note that `TableQuery` fetch operations can only be called on queries
 * originating from a `TableViewModel`, otherwise an error is thrown.
 * Thus, queries created via this function can only be used as an argument in
 * `TableViewModel` fetch calls:
 * ```
 * let idQuery = qb.query().select('objectId');
 * qb.table('Person').fetch(idQuery);
 * ```
 */
export function query(): TableQuery {
  return new TableQuery();
}


/*****************************************************************************
 * Invoke a Backendless service API method.
 * @param {string} serviceName Name of the deployed service
 * @param {string} methodName Name of the method to invoke
 * @param {object} [params] Object containing the parameters passed to the method.
 */
export function invoke(serviceName: string, methodName: string, params?: object) {
  return Backendless.APIServices.invoke(serviceName, methodName, params ?? {});
}