When building new components for quasar-backendless, be sure to:

- append an export in 'quasar-backendless/components/index.js'

- make a registration call for the component in 'quasar-backendless/boot/index.js' under 'registerComponents()'