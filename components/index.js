import QBTable from './QBTable'
import QBSelect from './QBSelect'

export {
  QBTable,
  QBSelect,
}