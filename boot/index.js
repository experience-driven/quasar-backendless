import {
  QBTable,
  QBSelect,
} from '../components'

// we globally register our components for the app
const registerComponents = function(app) {
  app.component('qb-table', QBTable);
  app.component('qb-select', QBSelect);
}

export default ({ app }) => {

  registerComponents(app);

}