import LoadingManager from './LoadingManager'
import { ComponentInternalInstance } from 'vue'


/**
 * Mapped component state for internal use.
 * @private This data is hidden from components and the rest of the application.
 */
export default class ComponentState {


  private m_instance: ComponentInternalInstance;
  public get instance() { return this.m_instance; }

  private m_loader:   LoadingManager;
  public get loader() { return this.m_loader; }


  constructor(instance: ComponentInternalInstance) {
    this.m_instance = instance;
    this.m_loader   = new LoadingManager();
  }


  public unmapped() {
    this.loader.clear();
  }


}