import { ref, Ref, ComponentInternalInstance } from 'vue'
import LoadingManager from './LoadingManager'
import ComponentState from './ComponentState'


/**
 * Quasar-Backendless state singleton.
 * Intended for internal use only.
 */
export default class State {


  private static instance: State;
  public static getInstance() {
    if (!State.instance) {
      State.instance = new State();
    }
    return State.instance;
  }


  private components:    Map<number, ComponentState>;
  private loader:        LoadingManager;
  private activeLoaders: number;
  private isLoading:     Ref<boolean>


  private constructor() {
    this.components    = new Map<number, ComponentState>();
    this.loader        = new LoadingManager();
    this.activeLoaders = 0;
    this.isLoading     = ref(false);
  }


  private log(...message: any[]) {
    if (!process.env.PROD)
      console.debug(`[State]\n`, ...message);
  }
  private warn(...message: any[]) {
    if (!process.env.PROD)
      console.warn(`[State]\n`, ...message);
  }
  private error(...message: any[]) {
    if (!process.env.PROD)
      console.error(`[State]\n`, ...message);
  }


  /***********************************************************************
   * Called externally by LoadingManager to signal global state that it
   * has changed activity states.
   */
   public updateLoaderActivity(loader: LoadingManager) {
    this.activeLoaders += loader.loading().value ? 1 : -1;
    if (this.activeLoaders < 0) {
      this.warn('activeLoaders', "somehow became negative (???)");
      this.activeLoaders = 0;
    }
    let active = this.activeLoaders > 0;
    if (active) {
      if (!this.isLoading.value)
        this.isLoading.value = true;
    } else {
      if (this.isLoading.value)
        this.isLoading.value = false;
    }
  }


  /***********************************************************************
   * Gets the mapped component state, or makes/returns a new one
   * if it doesn't exist.
   * @param instance The vue component instance.
   * @returns {ComponentState}
   */
  public mapped(instance: ComponentInternalInstance): ComponentState {
    let component = this.components.get(instance.uid);
    if (component != null)
      return component;
    component = new ComponentState(instance);
    this.components.set(instance.uid, component);
    return component;
  }


  /***************************************************************************
   * Deletes the mapped component state.
   * @param instance The vue component instance.
   */
  public unmap(instance: ComponentInternalInstance): void {
    const component = this.components.get(instance.uid);
    if (component == null)
      return;
    this.components.delete(instance.uid);
    component.unmapped();
  }


  /*****************************************************************************
   * Get or set the app's global loading state.
   * Setting or getting a key state applies only to the global loading state.
   * 
   * @param key If provided, check or set the state for that key.
   * If not provided, the function will return true if there is any active
   * loading state, either globally or in any mapped component.
   * @param state If provided, set the state for the provided key.
   * If not provided, the function will simply return the current state of
   * the key.
   * 
   * @returns Current state of given key. If no key is provided,
   * returns true if any state is active.
   */
  public loading(key?: string, state?: boolean): Ref<boolean> {
    if (key == null)
      return this.isLoading;
    if (state == null)
      return this.loader.get(key);
    return this.loader.set(key, state);
  }


  /*****************************************************************************
   * @returns Current loading state, exclusively global.
   */
  public loadingGlobalOnly(): Ref<boolean> {
    return this.loader.loading();
  }


}