import { reactive, toRef, ref, Ref } from 'vue'
import State from './State'

/*******************************************************************************
 * 
 * A reactive key-based loading state manager.
 * 
 */
export default class LoadingManager {


  private loadTable:   Record<string, boolean>;
  private isActive:    Ref<boolean>;
  private activeCount: number;


  constructor() {
    this.loadTable   = reactive({});
    this.isActive    = ref(false);
    this.activeCount = 0;
  }


  private log(...message: any[]) {
    if (!process.env.PROD)
      console.debug(`[LoadingManager]\n`, ...message);
  }
  private warn(...message: any[]) {
    if (!process.env.PROD)
      console.warn(`[LoadingManager]\n`, ...message);
  }
  private error(...message: any[]) {
    if (!process.env.PROD)
      console.error(`[LoadingManager]\n`, ...message);
  }


  /*****************************************************************************
   * Updates `isActive` by checking all states in `loadTable`, and updates
   * the global state of this loader's activity.
   */
  private updateActive(): void {
    let active = this.activeCount > 0;
    // only modify reactive if the value actually changes
    if (this.isActive.value != active) {
      this.isActive.value = active;
      State.getInstance().updateLoaderActivity(this);
    }
  }

  /*****************************************************************************
   * Set the loading state for a key.
   * 
   * @param key Key to set.
   * @param state Enables the state of `key` if `state === true`.
   * Otherwise, the key is disabled.
   * @returns The state of this key.
   */
  public set(key: string, state: boolean): Ref<boolean> {
    const ref = toRef(this.loadTable, key);
    if (state) {
      if (!ref.value) {
        ref.value = true;
        this.activeCount++;
      } else {
        this.warn(`attempted to enable active state '${key}'. (is this key being used somewhere else or forgot to disable prior?)`);
      }
    } else {
      if (ref.value) {
        ref.value = false;
        this.activeCount--;
      } else {
        this.warn(`attempted to disable inactive state '${key}'. (is this key being used somewhere else or forgot to enable prior?)`);
      }
    }
    this.updateActive();
    return ref;
  }

  /*****************************************************************************
   * Toggle the loading state for a key.
   * 
   * @param key Key to toggle state.
   * @returns The state of this key.
   */
  public toggle(key: string): Ref<boolean> {
    const ref = toRef(this.loadTable, key);
    if (ref.value) {
      ref.value = false;
      this.activeCount--;
    } else {
      ref.value = true;
      this.activeCount++;
    }
    this.updateActive();
    return ref;
  }


  /*****************************************************************************
   * Get the loading state for a key.
   * 
   * @param key Key to get loading state.
   * @returns The current state of this key.
   */
  public get(key: string): Ref<boolean> {
    let ref = toRef(this.loadTable, key);
    if (ref.value == null)
      ref.value = false;
    return ref;
  }


  /*****************************************************************************
   * Completely clears the loading state.
   */
  public clear(): void {
    let keys = Object.keys(this.loadTable);
    for (const key of keys) {
      let ref = toRef(this.loadTable, key);
      ref.value = false;
    }
    this.activeCount = 0;
    this.updateActive();
  }


  /*****************************************************************************
   * @returns List of keys with an enabled loading state.
   */
  public keys(): string[] {
    let activeKeys = [];
    for (const key of Object.keys(this.loadTable)) {
      let ref = toRef(this.loadTable, key);
      if (ref.value)
        activeKeys.push(key);
    }
    return activeKeys;
  }


  /*****************************************************************************
   * @returns Count of currently enabled loading states.
   */
  public count(): number {
    return this.activeCount;
  }


  /*****************************************************************************
     * Get or set the loading state.
     * 
     * @param key If provided, check or set the state for that key.
     * If not provided, the function will return true if there is any active
     * loading state.
     * @param state If also provided, set the state for the provided key.
     * If not provided, the function will simply return the current state of
     * the key.
     * 
     * @example
     * myLoader.loading("myTask", true) // Enables loading state for key "myTask".
     * myLoader.loading("myTask") // Get the loading state for "myTask".
     * myLoader.loading() // Returns true if there is any loading.
     * 
     * @returns Current state of given key. If no key is provided,
     * returns true if any loading state is active.
     */
  public loading(key?: string, state?: boolean): Ref<boolean> {
    if (key == null)
      return this.isActive;
    if (state == null)
      return this.get(key);
    return this.set(key, state);
  }


}