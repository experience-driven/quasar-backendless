import IQueryable from './IQueryable';
import TableQuery from './TableQuery'

/**
 * Represents a Table dataset that can be fetched from.
 */
export default class TableViewModel<
  DBModel = object,
  ViewModel extends DBModel = DBModel
> implements IQueryable {


  /**
   * Backendless data store.
   */
  private db: Backendless.DataStore;
  
  /**
   * Name of the Backendless data store table.
   */
  private dbName: string;

  /**
   * Base query for fetching as-is and query chaining.
   */
  private baseQuery = new TableQuery<DBModel, ViewModel>(this);

  /**
   * Where-clause condition applied to all fetch calls on this table.
   */
  private globalCondition?: string;

  /**
   * Callbacks on API requests.
   */
  private fetchCallbacks: ((task: Promise<any>, taskName: string)=>void)[] = [];


  private log(...message: any[]) {
    if (!process.env.PROD)
      console.debug(`${this.toString()}\n`, ...message);
  }
  private warn(...message: any[]) {
    if (!process.env.PROD)
      console.warn(`${this.toString()}\n`, ...message);
  }
  private error(...message: any[]) {
    if (!process.env.PROD)
      console.error(`${this.toString()}\n`, ...message);
  }


  /*****************************************************************************
   * @param tableName Name of the table to fetch from.
   */
  constructor(tableName: string) {
    this.db = Backendless.Data.of(this.dbName = tableName);
  }


  /*****************************************************************************
   * Get a presentable string of the query object.
   */
  public toString(): string {
    return `[TableViewModel:${this.dbName}]`;
  }


  /*****************************************************************************
   * Invoke onRequest callbacks.
   * @param task Promise of API invoke.
   * @param taskName Name of the task.
   */
  private emitRequest<T>(task: Promise<T>, taskName: string) {
    this.fetchCallbacks.forEach((fn:Function) => {
      fn(task, taskName);
    });
  }


  /*****************************************************************************
   * Push callback event when an API request has been made.
   * @param  fn
   * @returns Relieve function; call it to remove this callback.
   */
  public onRequest<T>(fn: (task: Promise<T>, taskName: string)=>void): ()=>void {
    const callbacks = this.fetchCallbacks;
    callbacks.push(fn);
    const pop = () => {
      const idx = callbacks.indexOf(fn);
      if (idx >= 0)
        callbacks.splice(idx, 1);
    }
    return pop;
  }


  /*****************************************************************************
   * Set a where-clause condition applied to all fetch calls on this table.
   */
  public setGlobalCondition(globalCondition: string): TableViewModel<DBModel, ViewModel> {
    this.globalCondition = globalCondition;
    return this;
  }


  /*****************************************************************************
   * Get the where-clause condition applied to all fetch calls on this table.
   */
  public getGlobalCondition(): string {
    return this.globalCondition ?? '';
  }


  /*****************************************************************************
   * Returns an array of column descriptors.
   * 
   * @returns Array of column descriptors in this format:  
   * ```js
   * [
   *   {
   *     autoLoad: true|false,
   *     customRegex: regex<string>,
   *     defaultValue: null|value<any>,
   *     isPrimaryKey: true|false,
   *     name: string,
   *     relatedTable: null|tableName<string>,
   *     required: true|false,
   *     type: dataType<string>
   *   },
   *   ...
   * ]
   * ```
   */
  public async describe() /* : Promise<object[]> */ {
    this.log("Describe");
    let task = Backendless.Data.describe(this.dbName);
    if (!process.env.PROD) {
      let self = this;
      task
        .then(result => self.log(`(Describe)`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'describe');
    return task;
  }


  /*****************************************************************************
   * Fetch records with query.
   * If no query is provided, all records are fetched (up to 100).
   * @param query Query to apply to the fetch call.
   */
  public async fetch(query?: TableQuery<DBModel, ViewModel>): Promise<ViewModel[]> {
    if (this.globalCondition != null) {
      query = (query ?? this.baseQuery).where(this.globalCondition);
    }
    this.log("Fetch", query?.getInfo() ?? "(no query)");
    let task = this.db.find<ViewModel>(query?.build());
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel[]) => self.log(`(Fetch) ${result.length} record(s) found`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetch');
    return task;
  }


  /*****************************************************************************
   * Fetch record by ID with query.
   * @param id Object ID string.
   * @param query Query to apply to the fetch call.
   */
  public async fetchById(id: string, query?: TableQuery<DBModel, ViewModel>): Promise<ViewModel> {
    if (this.globalCondition != null) {
      query = (query ?? this.baseQuery).where(this.globalCondition);
    }
    this.log("Fetch By ID", id, query?.getInfo() ?? "(no query)");
    let task = this.db.findById<ViewModel>(id, query?.build());
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel) => self.log(`(Fetch By ID) ${result ? '1' : '0'} record(s) found`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetchById');
    return task;
  }


  /*****************************************************************************
   * Fetch records by their IDs with query.
   * @param ids Array of object ID strings.
   * @param query Query to apply to the fetch call.
   */
  public async fetchByIds(ids: string[], query?: TableQuery<DBModel, ViewModel>): Promise<ViewModel[]> {
    if (this.globalCondition != null && query != null) {
      query = query.where(this.globalCondition);
    }
    this.log("Fetch By IDs", ids, query?.getInfo() ?? "(no query)");
    let idListStr = "'" + ids.join("','") + "'";
    let idClause = `(objectId IN (${idListStr}))`;
    if (query == null) {
      query = this.where(idClause).size(ids.length);
    } else {
      query = query.where(idClause).size(ids.length);
    }
    let task = this.db.find<ViewModel>(query?.build());
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel[]) => self.log(`(Fetch By IDs) ${result.length} record(s) found`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetchByIds');
    return task;
  }


  /*****************************************************************************
   * Fetch first record in query.
   * @param query Query to apply to the fetch call.
   */
  public async fetchFirst(query?: TableQuery<DBModel, ViewModel>): Promise<ViewModel> {
    if (this.globalCondition != null) {
      query = (query ?? this.baseQuery).where(this.globalCondition);
    }
    this.log("Fetch First", query?.getInfo() ?? "(no query)");
    let task = this.db.findFirst<ViewModel>(query?.build());
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel) => self.log(`(Fetch First) ${result ? '1' : '0'} record(s) found`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetchFirst');
    return task;
  }


  /*****************************************************************************
   * Fetch last record in query.
   * @param query Query to apply to the fetch call.
   */
  public async fetchLast(query?: TableQuery<DBModel, ViewModel>): Promise<ViewModel> {
    if (this.globalCondition != null) {
      query = (query ?? this.baseQuery).where(this.globalCondition);
    }
    this.log("Fetch Last", query?.getInfo() ?? "(no query)");
    let task = this.db.findLast<ViewModel>(query?.build());
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel) => self.log(`(Fetch Last) ${result ? '1' : '0'} record(s) found`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetchLast');
    return task;
  }


  /*****************************************************************************
   * Fetch number of records in query.
   * @param query Query to apply to the fetch call.
   */
  public async fetchCount(query?: TableQuery<DBModel, ViewModel>): Promise<number> {
    if (this.globalCondition != null) {
      query = (query ?? this.baseQuery).where(this.globalCondition);
    }
    this.log("Fetch Count", query?.getInfo() ?? "(no query)");
    let task = this.db.getObjectCount(query?.build());
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: number) => self.log(`(Fetch Count) ${result} record(s) found`))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetchCount');
    return task;
  }


  /*****************************************************************************
   * Fetch related records.
   * @param parent Parent ID or object with `objectId`.
   * @param column Relation column on parent table.
   * @param relationsQuery Optional query applied to fetched relations.
   * @example
   * ```
   * let fiveFriendsInCity = qb.query().where('city = "Toronto"').size(5);
   * qb.table('Person').fetchRelated(person, 'friends', fiveFriendsInCity);
   * ```
   * Note: `include`-related queries have no effect in this operation.
   */
  public async fetchRelated<T>(parent: string|object, column: string, relationsQuery?: TableQuery<DBModel, ViewModel>): Promise<T[]> {
    this.log("Fetch Related", parent, column, relationsQuery?.getInfo() ?? "(no query)");
    let query = Backendless.LoadRelationsQueryBuilder.create();
    if (relationsQuery != null) {
      relationsQuery.build(query);
    }
    query.setRelationName(column);
    let task = this.db.loadRelations<T>(parent, query);
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: T[]) => self.log(`(Fetch Related) ${result.length} record(s) found`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'fetchRelated');
    return task;
  }


  /*****************************************************************************
   * Save a record.
   * @param record A record object with or without an `objectId`.
   * If an `objectId` is not provided, a new object will be saved and returned with
   * its `objectId`.
   * @returns The saved/updated object.
   */
  public async save(record: DBModel): Promise<ViewModel> {
    this.log("Save", record);
    let task = this.db.save<DBModel>(record) as Promise<ViewModel>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel) => self.log(`(Save) ${result ? '1' : '0'} record(s) updated`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'save');
    return task;
  }


  /*****************************************************************************
   * Deep-save a record. This will save related records in their respective
   * tables and update relations on parent record.  
   * ***IMPORTANT:** Removing relations from given record object are ignored
   * and must be done manually. (i.e. Setting a 1:1 related field to `null` will
   * be ignored and not remove the relation, and elements given in a 1:N related field will
   * only add relations or update existing related records.)*
   * @param record A record object with or without an `objectId`.
   * If an `objectId` is not provided, a new object will be saved and returned
   * with its `objectId`. This rule also applies to the related records (up to
   * a max depth of 1).
   * @returns The saved/updated object.
   * @example
   * ```
   * let person = {
   *   // Sets new object to 1:1 relation
   *   homeAddress: { street: "123 na", city: "Toronto" },
   *   otherAddresses: [
   *     // Adds these new objects to 1:N relations
   *     { street: "345 na", city: "Toronto" },
   *     { street: "567 na", city: "Toronto" },
   *   ],
   * };
   * personTable.deepSave(person);
   * ```
   */
  public async deepSave(record: DBModel): Promise<ViewModel> {
    this.log("Deep Save", record);
    let task = this.db.deepSave(record) as Promise<ViewModel>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: ViewModel) => self.log(`(Deep Save) ${result ? '1' : '0'} record(s) updated`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'deepSave');
    return task;
  }


  /*****************************************************************************
   * Delete a record.
   * @param record An `objectId` string or a record object with an `objectId`.
   * @returns Timestamp of removal.
   */
  public async delete(record: DBModel|string) : Promise<object> {
    this.log("Delete", record);
    let task = this.db.remove(record as object|string);
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: object) => self.log(`(Delete) ${result ? '1' : '0'} record(s) deleted`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'delete');
    return task;
  }


  /*****************************************************************************
   * Add related children to parent record.  
   * **This is a concatenation operation.** For one-to-one relations, an error is
   * thrown if the parent object has a child for the specified column at the
   * time when the operation is called. Use `setRelation()` instead.
   * @param record Parent ID or object with `objectId`.
   * @param column Relation column on parent table.
   * @param children  
   * 1:1 - Child object or ID. (Or array with one element only)  
   * 1:N - Child object, ID, or array of child objects or IDs.  
   * If using objects, they must contain a valid `objectId`.  
   * @returns {Promise<number>} Number of child records added to the relation.
   */
  public async addRelation(record: DBModel|string, column: string, children: object|string|(object|string)[]): Promise<number> {
    this.log("Add Relation", record, column, children);
    if (!Array.isArray(children))
      children = [ children ];
    // backendless api's type signature for addRelation is inaccurate
    // unfortunately we have to ignore type safety for this call
    // api:     Backendless.DataStore.addRelation(parent: object, columnName: string, children: (string|object)[]): Promise<string>
    // actual:  Backendless.DataStore.addRelation(parent: object|string, columnName: string, children: object|string|(string|object)[]): Promise<number>
    let task = this.db.addRelation(record as any, column, children as any) as unknown as Promise<number>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: number) => self.log(`(Add Relation) ${result} record(s) updated`))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'addRelation');
    return task;
  }


  /*****************************************************************************
   * Sets related children to parent record.  
   * **This is a replacement operation.**
   * If the parent had other child objects prior to the operation, the relation
   * between them and the parent is removed.
   * @param record Parent ID or object with `objectId`.
   * @param column Relation column on parent table.
   * @param children
   * 1:1 - Child object or ID. (Or array with one element only)  
   * 1:N - Child object, ID, or array of child objects or IDs.  
   * If using objects, they must contain a valid `objectId`.  
   * @returns Number of child records set into the relation.
   */
  public async setRelation(record: DBModel|string, column: string, children: object|string|(object|string)[]): Promise<number> {
    this.log("Set Relation", record, column, children);
    if (!Array.isArray(children))
      children = [ children ];
    // backendless api's type signature for setRelation is inaccurate
    // unfortunately we have to ignore type safety for this call
    // api:     Backendless.DataStore.setRelation(parent: object, columnName: string, children: (string|object)[]): Promise<string>
    // actual:  Backendless.DataStore.setRelation(parent: object|string, columnName: string, children: object|string|(string|object)[]): Promise<number>
    let task = this.db.setRelation(record as any, column as any, children as any) as unknown as Promise<number>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: number) => self.log(`(Set Relation) ${result} record(s) updated`))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'setRelation');
    return task;
  }


  /*****************************************************************************
   * Delete related children on parent record based on a condition.  
   * @param record Parent ID or object with `objectId`.
   * @param column Relation column on parent table.
   * @param where Where-clause condition for selecting relations to be removed.
   * @returns Number of child records removed from the relation.
   */
  public async deleteRelation(record: DBModel|string, column: string, where: string): Promise<number>;


  /*****************************************************************************
   * Delete related children on parent record based on a condition.  
   * @param record Parent ID or object with `objectId`.
   * @param column Relation column on parent table.
   * @param where Where-clause condition for selecting relations to be removed.
   * @returns Number of child records removed from the relation.
   */
  public async deleteRelation(record: DBModel|string, column: string, children: (object|string)[]): Promise<number>


  /*****************************************************************************
   * Implements deleteRelation.
   */
  public async deleteRelation(record: DBModel|string, column: string, children: string|(object|string)[]): Promise<number> {
    this.log("Delete Relation", record, column, children);
    // backendless api's type signature for deleteRelation is inaccurate
    // unfortunately we have to ignore type safety for this call
    // api:     Backendless.DataStore.deleteRelation(parent: object, columnName: string, children: (string|object)[]): Promise<string>
    // actual:  Backendless.DataStore.deleteRelation(parent: object|string, columnName: string, children: string|(string|object)[]): Promise<number>
    let task = this.db.deleteRelation(record as any, column, children as any) as unknown as Promise<number>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: number) => self.log(`(Delete Relation) ${result} record(s) removed`))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'deleteRelation');
    return task;
  }


  /*****************************************************************************
   * Create many new records. (Maximum: 100)
   * @param records An array of record objects without an `objectId`.  
   * **Warning: Records with a `objectId` field will be rejected by the server!**
   * To update existing objects, use must use `save` on each record.
   * @returns Array of new IDs in order they
   * were provided.
   */
  public async bulkCreate(records: object[]): Promise<string[]> {
    this.log("Bulk Create", records);
    let task = this.db.bulkCreate(records);
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: string[]) => self.log(`(Bulk Create) ${result.length} record(s) created`, result))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'bulkCreate');
    return task;
  }


  /*****************************************************************************
   * Delete many records based on a condition.
   * @param {string} where A where-clause condition ( `string` )
   * @returns {} Number of records deleted.
   */
  public async bulkDelete(where: string): Promise<number>;


  /*****************************************************************************
   * Delete many records by ID.
   * @param {string[]} ids An array of object IDs ( `string[]` )
   * @returns {} Number of records deleted.
   */
  public async bulkDelete(ids: string[]): Promise<number>;


  /*****************************************************************************
   * Delete many records by object with `objectId`.
   * @param {object[]} records An array of record objects with an `objectId`
   * @returns {} Number of records deleted.
   */
  public async bulkDelete(records: object[]): Promise<number>;


  /*****************************************************************************
   * Implements bulkDelete.
   */
  public async bulkDelete(arg: string|string[]|object[]): Promise<number> {
    this.log("Bulk Delete", arg);
    // backendless api's return type for bulkDelete is inaccurate
    // unfortunately we have to ignore type safety for this call
    // api:     Promise<string>
    // actual:  Promise<number>
    let task = this.db.bulkDelete(arg as any) as unknown as Promise<number>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: number) => self.log(`(Bulk Delete) ${result} record(s) deleted`))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'bulkDelete');
    return task;
  }


  /*****************************************************************************
   * Update many records based on a condition.
   * @param condition Where-clause condition for selecting records to
   * be updated.
   * @param changes Object containing only the field-values to be
   * updated on all objects passing the condition.
   * @returns Number of records updated.
   */
  public async bulkUpdate(condition: string, changes: object): Promise<number>;


  /*****************************************************************************
   * Update many records by ID.
   * @param ids Array of object IDs for selecting records to be
   * updated.
   * @param changes Object containing only the field-values to be
   * updated on all objects with a matching ID.
   * @returns Number of records updated.
   */
   public async bulkUpdate(ids: string[], changes: object): Promise<number>;


  /*****************************************************************************
   * Implements bulkUpdate.
   */
  public async bulkUpdate(condition: string|string[], changes: object): Promise<number> {
    this.log("Bulk Update", condition, changes);
    if (Array.isArray(condition)) {
      condition = `(objectId IN ('${condition.join("','")}'))`;
    }
    // backendless api's return type for bulkUpdate is inaccurate
    // unfortunately we have to ignore type safety for this call
    // api:     Promise<string>
    // actual:  Promise<number>
    let task = this.db.bulkUpdate(condition, changes) as unknown as Promise<number>;
    if (!process.env.PROD) {
      let self = this;
      task
        .then((result: number) => self.log(`(Bulk Update) ${result} record(s) updated`))
        .catch(e => self.warn(e));
    }
    this.emitRequest(task, 'bulkUpdate');
    return task;
  }


  /*****************************************************************************
   * Use a conditional where-clause.
   * @param clause Where-clause. Ex.:  
   * `firstName = 'John' AND lastName = 'Smith'`
   */
  public where(clause: string): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.where(clause);
  }


  /*****************************************************************************
   * Select a column from table.
   * @param column Column to retrieve.
   */
  public select(column: string): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Select columns from table.
   * @param columns Columns to retrieve.
   */
  public select(columns: string[]): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements select.
   */
  public select(columns: string|string[]): TableQuery<DBModel, ViewModel>;
  public select(columns: string|string[]): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.select(columns);
  }


  /*****************************************************************************
   * Exclude a column from table.
   * @param column Column to exclude.
   */
  public exclude(column: string): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Exclude columns from table.
   * @param columns Columns to exclude.
   */
  public exclude(columns: string[]): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements exclude.
   */
  public exclude(columns: string|string[]): TableQuery<DBModel, ViewModel>;
  public exclude(columns: string|string[]): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.exclude(columns);
  }


  /*****************************************************************************
   * Include a related column from parent table.  
   * Relation page size is default 10. Use `includeSize()` to change.  
   * Relation depth is default 1. Use `includeDepth()` to change.
   * @param relatedColumn Related column to retrieve.
   */
  public include(relatedColumns: string): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Include related columns from parent table.  
   * Relation page size is default 10. Use `includeSize()` to change.  
   * Relation depth is default 1. Use `includeDepth()` to change.
   * @param relatedColumns Related columns to retrieve.
   */
  public include(relatedColumns: string[]): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements include.
   */
  public include(relatedColumns: string|string[]): TableQuery<DBModel, ViewModel>;
  public include(relatedColumns: string|string[]): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.include(relatedColumns);
  }


  /*****************************************************************************
   * Set page size of related columns from table.
   * Default is `10` when unprovided.
   * @param size Page size.
   */
  public includeSize(size: number): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.includeDepth(size);
  }


  /*****************************************************************************
   * Set retrieval depth of related columns from table. If no columns are included
   * with `include()`, all related columns within depth range will be included.
   * Default is `0` when unprovided.
   * @param depth Retrieval depth. (Maximum `10`)
   */
  public includeDepth(depth: number): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.includeDepth(depth);
  }


  /*****************************************************************************
   * Sort records by a column.
   * @param column Column name to sort by.
   * Append `DESC` to the column name for descending order. (Ex. `firstName DESC`)
   */
  public sort(column: string): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Sort records by columns.
   * @param columns Column names to sort by.
   * Append `DESC` to the column name for descending order. (Ex. `firstName DESC`)
   */
  public sort(columns: string[]): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements sort.
   */
  public sort(columns: string|string[]): TableQuery<DBModel, ViewModel>;
  public sort(columns: string|string[]): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.sort(columns);
  }


  /*****************************************************************************
   * Set pagination size.
   * @param size Page size.
   */
  public size(pageSize: number): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.size(pageSize);
  }


  /*****************************************************************************
   * Set pagination offset.
   * @param offset Page offset.
   */
  public offset(pageOffset: number): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.offset(pageOffset);
  }


  /*****************************************************************************
   * Set pagination size and offset in one go.
   * @param offset Page offset.
   * @param size Page size.
   */
  public offsetSize(pageOffset: number, pageSize: number): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.offsetSize(pageOffset, pageSize);
  }


  /*****************************************************************************
   * Group records by a column.
   * @param column Column name to group by.
   */
  public group(column: string): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Group records by columns.
   * @param columns Column names to group by.
   */
  public group(columns: string[]): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements group.
   */
  public group(columns: string|string[]): TableQuery<DBModel, ViewModel>;
  public group(columns: string|string[]): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.group(columns);
  }


  /*****************************************************************************
   * Use a conditional having-clause for grouped records.
   * @param clause Having-clause. Example: `COUNT(objectId) > 5`
   */
  public having(clause: string): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.having(clause);
  }


  /*****************************************************************************
   * Only recieve records/data with distinct values from selected columns.
   * @param enable Enable distinct mode.
   */
  public distinct(enable: boolean = true): TableQuery<DBModel, ViewModel> {
    return this.baseQuery.distinct(enable);
  }


}