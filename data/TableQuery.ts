import IQueryable from './IQueryable';
import TableViewModel from './TableViewModel'


class QueryInfo {

  whereClause?:        string;

  properties?:         string|string[];

  excludeProperties?:  string|string[];

  sortBy?:             string|string[];

  pageSize?:           number;

  pageOffset?:         number;

  related?:            string|string[];

  relatedPageSize?:    number;

  relatedDepth?:       number;

  groupBy?:            string|string[];

  havingClause?:       string;

  distinct?:           boolean;

};


/**
 * Allows the dynamic creation and reuse of queries.
 */
export default class TableQuery<
  DBModel = object,
  ViewModel extends DBModel = DBModel
> implements IQueryable {


  private info = new QueryInfo();

  private table?: TableViewModel<DBModel, ViewModel>;

  private builder?: Backendless.DataQueryBuilder;


  /*****************************************************************************
   * @param parent Parent table or query to chain off from.
   */
  constructor(parent?: TableViewModel<DBModel, ViewModel>|TableQuery<DBModel, ViewModel>) {
    if (parent instanceof TableQuery) {
      if (parent?.info != null)
        Object.assign(this.info, parent.info);
      this.table = parent?.table;
    } else if (parent instanceof TableViewModel) {
      this.table = parent;
    }
  }


  /*****************************************************************************
   * Get a presentable string of the query object.
   */
  public toString(): string {
    return JSON.stringify(this.info);
  }


  /*****************************************************************************
   * Get a copy of the query info object.
   */
  public getInfo(): object {
    return Object.assign(new QueryInfo, this.info);
  }


  /*****************************************************************************
   * Build a Backendless DataQueryBuilder object from this TableQuery.
   * 
   * @param target If provided, query properties are
   * applied to `target` instead of this query's internal builder. Return value
   * becomes the value of `target`.
   */
  public build(target?: Backendless.DataQueryBuilder): Backendless.DataQueryBuilder {
    if (this.builder != null && target == null)
      return this.builder;

    const {
      whereClause,  properties,    excludeProperties,
      sortBy,       pageSize,      pageOffset,
      related,      relatedDepth,  relatedPageSize,
      groupBy,      havingClause,  distinct
    } = this.info;

    let builder = target ?? Backendless.DataQueryBuilder.create();
    whereClause        && builder.setWhereClause(whereClause);
    properties         && builder.setProperties(properties);
    sortBy             && builder.setSortBy(sortBy);
    pageSize           && builder.setPageSize(pageSize);
    pageOffset         && builder.setOffset(pageOffset);
    excludeProperties  && builder.excludeProperties(excludeProperties);
    related            && builder.setRelated(related);
    relatedDepth       && builder.setRelationsDepth(relatedDepth);
    relatedPageSize    && builder.setRelationsPageSize(relatedPageSize);
    groupBy            && builder.setGroupBy(groupBy);
    havingClause       && builder.setHavingClause(havingClause);
    distinct           && builder.setDistinct(distinct);

    return target ?? (this.builder = builder);
  }


  /*****************************************************************************
   * Fetch records with this query.
   * If no query is provided, all records are fetched (up to 100).
   * *Can only be called if this query originated from a TableViewModel.
   * Alternatively, you can pass this query into a TableViewModel fetcher.*
   */
  public async fetch(): Promise<ViewModel[]> {
    if (this.table == null)
      throw new Error("TableQuery.fetch can only be called in a chain originating from a TableViewModel.");
    return this.table.fetch(this);
  }


  /*****************************************************************************
   * Fetch record by ID with this query.  
   * *Can only be called if this query originated from a TableViewModel.
   * Alternatively, you can pass this query into a TableViewModel fetcher.*
   * @param id Object ID string.
   */
  public async fetchById(id: string): Promise<ViewModel> {
    if (this.table == null)
      throw new Error("TableQuery.fetchById can only be called in a chain originating from a TableViewModel.");
    return this.table.fetchById(id, this);
  }


  /*****************************************************************************
   * Fetch records by their IDs with this query.  
   * *Can only be called if this query originated from a TableViewModel.
   * Alternatively, you can pass this query into a TableViewModel fetcher.*
   * @param ids Array of object ID strings.
   */
  public async fetchByIds(ids: string[]): Promise<ViewModel[]> {
    if (this.table == null)
      throw new Error("TableQuery.fetchByIds can only be called in a chain originating from a TableViewModel.");
    return this.table.fetchByIds(ids, this);
  }


  /*****************************************************************************
   * Fetch first object in this query.  
   * *Can only be called if this query originated from a TableViewModel.
   * Alternatively, you can pass this query into a TableViewModel fetcher.*
   */
  public async fetchFirst(): Promise<ViewModel> {
    if (this.table == null)
      throw new Error("TableQuery.fetchFirst can only be called in a chain originating from a TableViewModel.");
    return this.table.fetchFirst(this);
  }


  /*****************************************************************************
   * Fetch last object in this query.  
   * *Can only be called if this query originated from a TableViewModel.
   * Alternatively, you can pass this query into a TableViewModel fetcher.*
   */
  public async fetchLast(): Promise<ViewModel> {
    if (this.table == null)
      throw new Error("TableQuery.fetchLast can only be called in a chain originating from a TableViewModel.");
    return this.table.fetchLast(this);
  }


  /*****************************************************************************
   * Fetch number of records in this query.  
   * *Can only be called if this query originated from a TableViewModel.
   * Alternatively, you can pass this query into a TableViewModel fetcher.*
   */
  public async fetchCount(): Promise<number> {
    if (this.table == null)
      throw new Error("TableQuery.fetchCount can only be called in a chain originating from a TableViewModel.");
    return this.table.fetchCount(this);
  }


  /*****************************************************************************
   * Add a conditional where-clause.  
   * If a where-clause was previously set, this will 'AND'-append the new clause.
   * Set `bReplace: true` to override this functionality.
   * @param {string} clause Where-clause. Ex.:  
   * `firstName = 'John' AND lastName = 'Smith'`
   * @param {boolean} [bReplace]
   * `true` will replace the current clause. (Default: false)
   */
  public where(clause:any, bReplace?:any): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.whereClause == null || bReplace) {
      query.info.whereClause = clause;
    } else {
      if (clause == null)
        return query;
      let orig = query.info.whereClause;
      query.info.whereClause = `(${orig}) AND (${clause})`;
    }
    return query;
  }


  /*****************************************************************************
   * Select a column from table.  
   * If a selection was previously set, this operation will append given
   * column(s) to the current selection.
   * Set `bReplace: true` to override this functionality.
   * @param column Column to retrieve.
   * @param bReplace `true` will replace all current selections.
   */
  public select(column: string|string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Select columns from table.  
   * If a selection was previously set, this operation will append given
   * column(s) to the current selection.
   * Set `bReplace: true` to override this functionality.
   * @param columns Columns to retrieve.
   * @param bReplace `true` will replace all current selections.
   */
  public select(columns: string|string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements select.
   */
  public select(columns: string|string[], bReplace: boolean = false): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.properties == null || bReplace) {
      query.info.properties = columns;
    } else {
      if (columns == null)
        return query;
      if (!Array.isArray(query.info.properties))
        query.info.properties = [ query.info.properties ];
      if (!Array.isArray(columns))
        columns = [ columns ];
      query.info.properties.push(...columns);
    }
    return query;
  }


  /*****************************************************************************
   * Exclude a column from table.  
   * If exclusions were previously set, this operation will append given
   * column(s) to current exclusions.
   * Set `bReplace: true` to override this functionality.
   * @param column Column to exclude.
   * @param bReplace `true` will replace all current exclusions.
   */
  public exclude(column: string, bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Exclude columns from table.  
   * If exclusions were previously set, this operation will append given
   * column(s) to current exclusions.
   * Set `bReplace: true` to override this functionality.
   * @param columns Columns to exclude.
   * @param bReplace `true` will replace all current exclusions.
   */
  public exclude(columns: string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements exclude.
   */
  public exclude(columns: string|string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;
  public exclude(columns: string|string[], bReplace: boolean = false): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.excludeProperties == null || bReplace) {
      query.info.excludeProperties = columns;
    } else {
      if (columns == null)
        return query;
      if (!Array.isArray(query.info.excludeProperties))
        query.info.excludeProperties = [ query.info.excludeProperties ];
      if (!Array.isArray(columns))
        columns = [ columns ];
      query.info.excludeProperties.push(...columns);
    }
    return query;
  }


  /*****************************************************************************
   * Include a related column from parent table.  
   * Relation page size is default 10. Use `includeSize()` to change.  
   * Relation depth is default 1. Use `includeDepth()` to change.  
   * If inclusions were previously set, this operation will append given
   * column(s) to current inclusions.
   * Set `bReplace: true` to override this functionality.
   * @param relatedColumn Related column to retrieve.
   * @param bReplace `true` will replace all current inclusions.
   */
   public include(relatedColumn: string, bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Include related columns from parent table.  
   * Relation page size is default 10. Use `includeSize()` to change.  
   * Relation depth is default 1. Use `includeDepth()` to change.  
   * If inclusions were previously set, this operation will append given
   * column(s) to current inclusions.
   * Set `bReplace: true` to override this functionality.
   * @param relatedColumns Related columns to retrieve.
   * @param bReplace `true` will replace all current inclusions.
   */
   public include(relatedColumns: string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements include.
   */
  public include(relatedColumns: string|string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;
  public include(relatedColumns: string|string[], bReplace: boolean = false): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.related == null || bReplace) {
      query.info.related = relatedColumns;
    } else {
      if (relatedColumns == null)
        return query;
      if (!Array.isArray(query.info.related))
        query.info.related = [ query.info.related ];
      if (!Array.isArray(relatedColumns))
        relatedColumns = [ relatedColumns ];
      query.info.related.push(...relatedColumns);
    }
    return query;
  }


  /*****************************************************************************
   * Set page size of related columns from table.
   * Default is `10` when unprovided.
   * @param size Page size.
   */
  public includeSize(size: number): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    (size != null) && (query.info.relatedPageSize = size);
    return query;
  }


  /*****************************************************************************
   * Set retrieval depth of related columns from table. If no columns are included
   * with `include()`, all related columns within depth range will be included.
   * Default is `0` when unprovided.
   * @param depth Retrieval depth. (Maximum `10`)
   */
  public includeDepth(depth: number): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    (depth != null) && (query.info.relatedDepth = depth);
    return query;
  }


  /*****************************************************************************
   * Sort records by a column.  
   * If sorting was previously set, this operation will append given
   * column(s) to current sortings.
   * Set `bReplace: true` to override this functionality.
   * @param column Column name to sort by.
   * Append `DESC` to the column name for descending order. (Ex. `firstName DESC`)
   * @param bReplace `true` will replace all current sortings.
   */
  public sort(column: string, bReplace?: boolean): TableQuery<DBModel, ViewModel>


  /*****************************************************************************
   * Sort records by columns.  
   * If sorting was previously set, this operation will append given
   * column(s) to current sortings.
   * Set `bReplace: true` to override this functionality.
   * @param columns Column names to sort by.
   * Append `DESC` to the column name for descending order. (Ex. `firstName DESC`)
   * @param bReplace `true` will replace all current sortings.
   */
  public sort(columns: string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>


  /*****************************************************************************
   * Implements sort.
   */
  public sort(columns: string|string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;
  public sort(columns: string|string[], bReplace: boolean = false): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.sortBy == null || bReplace) {
      query.info.sortBy = columns;
    } else {
      if (columns == null)
        return query;
      if (!Array.isArray(query.info.sortBy))
        query.info.sortBy = [ query.info.sortBy ];
      if (!Array.isArray(columns))
        columns = [ columns ];
      query.info.sortBy.push(...columns);
    }
    return query;
  }


  /*****************************************************************************
   * Set pagination size.
   * @param size
   */
  public size(size: number): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    size && (query.info.pageSize = size);
    return query;
  }


  /*****************************************************************************
   * Set pagination offset.
   * @param offset
   */
  public offset(offset: number): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    offset && (query.info.pageOffset = offset);
    return query;
  }


  /*****************************************************************************
   * Set pagination size and offset.
   * @param offset
   * @param size
   */
  public offsetSize(offset: number, size: number): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    offset && (query.info.pageOffset = offset);
    size   && (query.info.pageSize   = size);
    return query;
  }


  /*****************************************************************************
   * Group records by a column.  
   * If groupings were previously set, this operation will append given
   * column(s) to current groupings.
   * Set `bReplace: true` to override this functionality.
   * @param column Column name to group by.
   * @param bReplace `true` will replace all current groupings.
   */
  public group(column: string, bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Group records by column(s).  
   * If groupings were previously set, this operation will append given
   * column(s) to current groupings.
   * Set `bReplace: true` to override this functionality.
   * @param columns Column names to group by.
   * @param bReplace `true` will replace all current groupings.
   */
  public group(columns: string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;


  /*****************************************************************************
   * Implements group.
   */
  public group(columns: string|string[], bReplace?: boolean): TableQuery<DBModel, ViewModel>;
  public group(columns: string|string[], bReplace: boolean = false): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.groupBy == null || bReplace) {
      query.info.groupBy = columns;
    } else {
      if (columns == null)
        return query;
      if (!Array.isArray(query.info.groupBy))
        query.info.groupBy = [ query.info.groupBy ];
      if (!Array.isArray(columns))
        columns = [ columns ];
      query.info.groupBy.push(...columns);
    }
    return query;
  }


  /*****************************************************************************
   * Use a conditional having-clause for grouped records.  
   * If a having-clause was previously set, this will 'AND'-append the new clause.
   * Set `bReplace: true` to override this functionality.
   * @param clause Having-clause. Example: `COUNT(objectId) > 5`
   * @param bReplace `true` will replace the current clause.
   */
  public having(clause: string, bReplace: boolean = false): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    if (query.info.havingClause == null || bReplace) {
      query.info.havingClause = clause;
    } else {
      if (clause == null)
        return query;
      let orig = query.info.havingClause;
      query.info.havingClause = `(${orig}) AND (${clause})`;
    }
    return query;
  }


  /*****************************************************************************
   * Only recieve records/data with distinct values from selected columns.
   * @param enable Enable distinct mode.
   */
  public distinct(enable: boolean = true): TableQuery<DBModel, ViewModel> {
    let query = new TableQuery(this);
    query.info.distinct = enable;
    return query;
  }


}