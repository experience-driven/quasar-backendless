/**
 * Provides utilities necessary to create a chainable query.
 */
export default interface IQueryable {


  /*****************************************************************************
   * Get a presentable string of the query object.
   */
  toString(): string;


  /*****************************************************************************
   * Use a conditional where-clause.
   * @param clause Where-clause. Ex.:  
   * `firstName = 'John' AND lastName = 'Smith'`
   */
   where(clause: string): IQueryable;


  /*****************************************************************************
   * Select a column from table.
   * @param column Column to retrieve.
   */
  select(column: string): IQueryable;


  /*****************************************************************************
   * Select columns from table.
   * @param columns Columns to retrieve.
   */
  select(columns: string[]): IQueryable;


  /*****************************************************************************
   * Exclude a column from table.
   * @param column Column to exclude.
   */
  exclude(column: string): IQueryable;


  /*****************************************************************************
   * Exclude columns from table.
   * @param columns Columns to exclude.
   */
  exclude(columns: string[]): IQueryable;


  /*****************************************************************************
   * Include a related column from parent table.  
   * Relation page size is default 10. Use `includeSize()` to change.  
   * Relation depth is default 1. Use `includeDepth()` to change.
   * @param relatedColumn Related column to retrieve.
   */
  include(relatedColumn: string): IQueryable;


  /*****************************************************************************
   * Include related columns from parent table.  
   * Relation page size is default 10. Use `includeSize()` to change.  
   * Relation depth is default 1. Use `includeDepth()` to change.
   * @param relatedColumns Related columns to retrieve.
   */
  include(relatedColumns: string[]): IQueryable;


  /*****************************************************************************
   * Set page size of related columns from table.
   * Default is `10` when unprovided.
   * @param size Page size.
   */
  includeSize(size: number): IQueryable;


  /*****************************************************************************
   * Set retrieval depth of related columns from table. If no columns are included
   * with `include()`, all related columns within depth range will be included.
   * Default is `0` when unprovided.
   * @param depth Retrieval depth. (Maximum `10`)
   */
  includeDepth(depth: number): IQueryable;


  /*****************************************************************************
   * Sort records by a column.
   * @param column Column name to sort by.
   * Append `DESC` to the column name for descending order. (Ex. `firstName DESC`)
   */
  sort(column: string): IQueryable;


  /*****************************************************************************
   * Sort records by columns.
   * @param columns Column names to sort by.
   * Append `DESC` to the column name for descending order. (Ex. `firstName DESC`)
   */
  sort(columns: string[]): IQueryable;


  /*****************************************************************************
   * Set pagination size.
   * @param size Page size.
   */
  size(size: number): IQueryable;


  /*****************************************************************************
   * Set pagination offset.
   * @param offset Page offset.
   */
  offset(offset: number): IQueryable;


  /*****************************************************************************
   * Set pagination size and offset in one go.
   * @param offset Page offset.
   * @param size Page size.
   */
  offsetSize(offset: number, size: number): IQueryable;


  /*****************************************************************************
   * Group records by a column.
   * @param column Column name to group by.
   */
  group(column: string): IQueryable;


  /*****************************************************************************
   * Group records by columns.
   * @param columns Column names to group by.
   */
  group(columns: string[]): IQueryable;


  /*****************************************************************************
   * Use a conditional having-clause for grouped records.
   * @param clause Having-clause. Example: `COUNT(objectId) > 5`
   */
  having(clause: string): IQueryable;


  /*****************************************************************************
   * Only recieve records/data with distinct values from selected columns.
   * @param enable Enable distinct mode.
   */
  distinct(enable?: boolean): IQueryable;


}